#!/usr/bin/env ruby
# Author: NWOPS, LLC <automation@nwops.io>
# Purpose: Convert the puppetfile to a .fixtures.yml file
# Date: 6/26/19
# exits 1 if Puppetfile is successful, 0 otherwise
# Dumps converted Puppetfile to STDOUT
# Usage: ./convert_puppetfile.rb [path_to_puppetfile]
# Note: only works with git modules ATM

require 'tempfile'
require 'yaml'

# @return [Array] - returns a array of hashes that contain modules with a git source
def git_modules(puppetfile)
  modules.find_all do |mod|
    mod[:args].keys.include?(:git)
  end
end

class String
  def strip_comment(markers = ['#', "\n"])
    re = Regexp.union(markers)
    index = (self =~ re)
    index.nil? ? rstrip : self[0, index].rstrip 
  end
end

# @param puppetfile [String] - the absolute path to the puppetfile
# @return [Array] - returns an array of module hashes that represent the puppetfile
# @example
# [{:namespace=>"puppetlabs", :name=>"stdlib", :args=>[]},
# {:namespace=>"petems", :name=>"swap_file", :args=>["'4.0.0'"]}]
def modules(puppetfile = File.expand_path('./Puppetfile'))
  @modules ||= begin
    return [] unless File.exist?(puppetfile)
    all_lines = File.read(puppetfile).lines.map(&:strip_comment)
    # remove comments from all the lines
    lines_without_comments = all_lines.reject {|line| line.match(/#.*\n/) }.join("\n").gsub(/\n/,'')
    lines_without_comments.split('mod').map do |line|
      next nil if line =~ /^forge/
      next nil if line.empty?
      parse_module_args(line)
    end.compact.uniq
  end
end



# @param data [String] - the string to parse the puppetfile args out of
# @return [Array] -  an array of arguments in hash form
# @example
# {:namespace=>"puppetlabs", :name=>"stdlib", :args=>[]}
# {:namespace=>"petems", :name=>"swap_file", :args=>["'4.0.0'"]}
def parse_module_args(data)
  args = data.split(',').map(&:strip)
  # we can't guarantee that there will be a namespace when git is used
  # remove quotes and dash and slash
  namespace, name = args.shift.gsub(/'|"/, '').split(/-|\//)
  name ||= namespace
  namespace = nil if namespace == name
  {
      namespace: namespace,
      name: name,
      args: process_args(args)
  }
end

# @return [Array] - returns an array of hashes with the args in key value pairs
# @param [Array] - the arguments processed from each entry in the puppetfile
# @example
# [{:args=>[], :name=>"razor", :namespace=>"puppetlabs"},
#  {:args=>[{:version=>"0.0.3"}], :name=>"ntp", :namespace=>"puppetlabs"},
#  {:args=>[], :name=>"inifile", :namespace=>"puppetlabs"},
#  {:args=>
#    [{:git=>"https://github.com/nwops/reportslack.git"}, {:ref=>"1.0.20"}],
#   :name=>"reportslack",
#   :namespace=>"nwops"},
#  {:args=>{:git=>"git://github.com/puppetlabs/puppetlabs-apt.git"},
#   :name=>"apt",
#   :namespace=>nil}
# ]
def process_args(args)
  results = {}
  args.each do |arg|
    a = arg.gsub(/'|"/, '').split(/\A\:|\:\s|\=\>/).map(&:strip).reject(&:empty?)
    if a.count < 2
      results[:version] = a.first
    else
      results[a.first.to_sym] = a.last
    end
  end
  results
end


puppetfile = ARGV.first || File.expand_path('./Puppetfile')

unless File.exist?(puppetfile)
  puts "puppetfile does not exist"
  puts "💩"
  exit 1
end

def fixtures_base
  {
    'fixtures' => {
      'repositories' => {

      }
    }
  }
end

all_modules = git_modules(puppetfile).reduce(fixtures_base) do |acc, mod|
  ref = mod[:args][:ref] || mod[:args][:tag] || mod[:args][:branch]
  acc['fixtures']['repositories'][mod[:name]] = {
      'repo' => mod[:args][:git],
      'ref'  => ref,
  }
  acc
end

#sorted = all_modules.sort_by {|a| a[:name] ? 1 : 0 }
puts all_modules.to_yaml
exit 0